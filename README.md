# Drupal Programming Task

### In order to be considered for a Drupal position, you must complete the following steps.
*Note: This task should take no longer than 1 hour at the most to complete.*


## Task

- Fork this repository.

- Create a custom module that:

    1. adds a menu item to the admin menu labeled as "Cleanse Pricing"
    2. when an admin clicks on that menu item, a simple form with just 1 field named "Cleanse Price" is displayed. This should store a commerce_price amount. The default value is $10 USD.
    3. handle saving and retrieving this variable

- Create a rule that uses this "Cleanse Price" as the unit price for product line items that have a custom field named "field_special_price" set to TRUE.

- Export the "Cleanse Price" value and the rule as a Feature

If you have any questions, please read carefully, then think about what would be the better approach, and go for it.

## Once Complete

1. Commit everything to the repo. Use whatever file/directory organization you prefer.
2. Send a pull request.
3. We will review your pull request, and get back to you with next steps.
