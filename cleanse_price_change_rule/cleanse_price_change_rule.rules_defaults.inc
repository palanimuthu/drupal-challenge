<?php
/**
 * @file
 * cleanse_price_change_rule.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function cleanse_price_change_rule_default_rules_configuration() {
  $items = array();
  $items['rules_cleanse_price_change'] = entity_import('rules_config', '{ "rules_cleanse_price_change" : {
      "LABEL" : "Cleanse Price change",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "php", "commerce_line_item", "commerce_product_reference" ],
      "ON" : { "commerce_product_calculate_sell_price" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:commerce-product" ],
            "field" : "field_special_price"
          }
        },
        { "data_is" : {
            "data" : [ "commerce-line-item:commerce-product:field-special-price" ],
            "value" : 1
          }
        }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : {
              "select" : "commerce-line-item:commerce-unit-price:amount",
              "num_offset" : { "value" : "2" },
              "php" : { "code" : "$query = db_select(\\u0027product_special_price\\u0027, \\u0027psp\\u0027)\\r\\n\\t  -\\u003Efields(\\u0027psp\\u0027, array(\\u0027price\\u0027));\\r\\n$value= $query-\\u003Eexecute()-\\u003EfetchField();\\r\\nreturn $value*100;" }
            },
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  return $items;
}
